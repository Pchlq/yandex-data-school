import numpy

n = int(input())
sup = 0
x = 0

for i in range (n):
    x = x + 1/n
    new = abs(x*x - x*x*x)
    if (new > sup):
        sup = new

print (round(sup, 4))
