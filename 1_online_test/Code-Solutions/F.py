import numpy
import math

a, b, r = map(int, input().split())
Sw = 1/4 * math.pi * r * r
Sr = a * b

res = Sw / Sr

# for i in range (1, n):
    # res = res + i / (n*n)

print (round(res, 4))
