#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>

void print_all_permutations(size_t permutation_size) {
	std::vector<int> permutation;
	for (size_t idx = 0; idx < permutation_size; ++idx) {
		permutation.push_back(idx);
	}

	std::copy(permutation.begin(), permutation.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	while (std::next_permutation(permutation.begin(), permutation.end())) {
		std::copy(permutation.begin(), permutation.end(), std::ostream_iterator<int>(std::cout, " "));
		std::cout << std::endl;
	}
}

bool can_do_step(const std::vector<int> & sequence,
		const std::vector<int> & directions,
		size_t sequence_pos,
		int sequence_max_value) {
	return !((directions[sequence_pos] == 1 && sequence[sequence_pos] == sequence_max_value) ||
			(directions[sequence_pos] == -1 && sequence[sequence_pos] == 1));
}

void do_step (
	std::vector<int> * sequence,
	std::vector<int> * directions,
	size_t sequence_pos,
	size_t sequence_size) {

	(*sequence)[sequence_pos] += (*directions)[sequence_pos];

	for (size_t sequence_idx = sequence_pos + 1; sequence_idx < sequence_size; ++sequence_idx) {
		(*directions)[sequence_idx] *= -1;
	}
}

bool get_next_sequence_by_one_position_diff(
		std::vector<int> * sequence,
		std::vector<int> * directions,
		size_t sequence_size,
		int sequence_max_value) {

	size_t sequence_pos(sequence_size - 1);
	while ((sequence_pos > 0) && !can_do_step(*sequence, *directions, sequence_pos, sequence_max_value)) {
		--sequence_pos;
	}

	if (can_do_step(*sequence, *directions, sequence_pos, sequence_max_value)) {
		do_step(sequence, directions, sequence_pos, sequence_size);
		return true;
	} else {
		return false;
	}
}

void print_all_sequences_by_one_position_diff(size_t sequence_max_value, size_t sequence_size) {
	std::vector<int> sequence(sequence_size, 1);
	std::vector<int> directions(sequence_size, 1);

	std::copy(sequence.begin(), sequence.end(), std::ostream_iterator<int>(std::cout, " "));
	std::cout << std::endl;

	while (get_next_sequence_by_one_position_diff(&sequence, &directions, sequence_size, sequence_max_value)) {
		std::copy(sequence.begin(), sequence.end(), std::ostream_iterator<int>(std::cout, " "));
		std::cout << std::endl;
	}
}

int main() {
	print_all_sequences_by_one_position_diff(2, 4);
	return 0;
}
